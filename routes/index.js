var express = require('express');
var router = express.Router();

var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/');
};

var isUnauthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isUnauthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/home');
};

module.exports = function (passport) {

    router.get('/', isUnauthenticated, function (req, res) {
        res.render('index', {
            message: req.flash('message'),
            title: "multiGame"
        });
    });

    router.get('/home', isAuthenticated, function (req, res) {
        res.render('home', {user: req.user});
    });

    router.get('/signout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    router.get('/auth/steam', passport.authenticate('steam'));

    router.get('/auth/steam/return',
        passport.authenticate('steam', {
            successRedirect: '/home',
            failureRedirect: '/'
        })
    );

    return router;
}